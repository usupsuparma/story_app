// To parse this JSON data, do
//
//     final storyDetailResult = storyDetailResultFromJson(jsonString);

import 'dart:convert';

import 'package:story_app/models/story.dart';

StoryDetailResult storyDetailResultFromJson(String str) =>
    StoryDetailResult.fromJson(json.decode(str));

String storyDetailResultToJson(StoryDetailResult data) =>
    json.encode(data.toJson());

class StoryDetailResult {
  bool error;
  String message;
  Story story;

  StoryDetailResult({
    required this.error,
    required this.message,
    required this.story,
  });

  factory StoryDetailResult.fromJson(Map<String, dynamic> json) =>
      StoryDetailResult(
        error: json["error"],
        message: json["message"],
        story: Story.fromJson(json["story"]),
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "story": story.toJson(),
      };
}
