class Session {
  String userId;
  String name;
  String token;

  Session({
    required this.userId,
    required this.name,
    required this.token,
  });

  factory Session.fromJson(Map<String, dynamic> json) => Session(
        userId: json["userId"],
        name: json["name"],
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "name": name,
        "token": token,
      };
}
