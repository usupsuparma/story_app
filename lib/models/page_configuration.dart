class PageConfiguration {
  final bool unknown;
  final bool register;
  final bool? loggedIn;
  final bool? addStory;
  final String? quoteId;

  PageConfiguration.splash()
      : unknown = false,
        register = false,
        loggedIn = null,
        quoteId = null,
        addStory = false;

  PageConfiguration.login()
      : unknown = false,
        register = false,
        loggedIn = false,
        addStory = false,
        quoteId = null;

  PageConfiguration.register()
      : unknown = false,
        register = true,
        loggedIn = false,
        addStory = false,
        quoteId = null;

  PageConfiguration.home()
      : unknown = false,
        register = false,
        loggedIn = true,
        addStory = false,
        quoteId = null;
  PageConfiguration.addStory()
      : unknown = false,
        register = false,
        loggedIn = true,
        addStory = true,
        quoteId = null;

  PageConfiguration.detailStory(String id)
      : unknown = false,
        register = false,
        loggedIn = true,
        addStory = false,
        quoteId = id;

  PageConfiguration.unknown()
      : unknown = true,
        register = false,
        loggedIn = null,
        addStory = false,
        quoteId = null;

  bool get isSplashPage => unknown == false && loggedIn == null;
  bool get isLoginPage => unknown == false && loggedIn == false;
  bool get isHomePage =>
      unknown == false && loggedIn == true && quoteId == null;
  bool get isDetailPage =>
      unknown == false && loggedIn == true && quoteId != null;
  bool get isRegisterPage => register == true;
  bool get isUnknownPage => unknown == true;
  bool get isAddStoryStory => addStory == true;
}
