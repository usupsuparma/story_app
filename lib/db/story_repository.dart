import 'package:story_app/models/story.dart';
import 'package:story_app/responses/response_story.dart';
import 'package:story_app/services/api_services.dart';

abstract class StoryRepository {
  Future<List<Story>> getStories();
  Future<ResponseResult> addNewStory(
    List<int> bytes,
    String fileName,
    String description,
  );
}

class StoryRepositoryImpl implements StoryRepository {
  ApiService apiService;
  StoryRepositoryImpl({required this.apiService});

  @override
  Future<List<Story>> getStories() async {
    try {
      print('test get story');
      ResponseStoriesResult responseStoriesResult =
          await apiService.getAllStories();
      print(responseStoriesResult.message);
      if (responseStoriesResult.error) {
        throw Exception(responseStoriesResult.message);
      }
      return responseStoriesResult.listStory;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<ResponseResult> addNewStory(
      List<int> bytes, String fileName, String description) async {
    try {
      return apiService.addNewStory(bytes, fileName, description);
    } catch (e) {
      rethrow;
    }
  }
}
