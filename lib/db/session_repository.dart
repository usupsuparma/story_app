import 'package:shared_preferences/shared_preferences.dart';
import 'package:story_app/models/session.dart';

abstract class SessionRepository {
  Future<bool> createSession(Session session);
  Future<Session> getSession();
  Future<String> getToken();
  Future<bool> isLogin();
  Future<bool> clearSession();
}

class SessionRepositoryImpl implements SessionRepository {
  final SharedPreferences preferences;

  final String userIdKey = "user-id";
  final String nameKey = "name-key";
  final String tokenKey = "token-key";

  SessionRepositoryImpl({
    required this.preferences,
  });

  @override
  Future<bool> createSession(Session session) async {
    try {
      await preferences.setString(nameKey, session.name);
      await preferences.setString(userIdKey, session.userId);
      await preferences.setString(tokenKey, session.token);
      return true;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Session> getSession() async {
    try {
      String userId = preferences.getString(userIdKey)!;
      String name = preferences.getString(nameKey)!;
      String token = preferences.getString(tokenKey)!;
      return Session(userId: userId, name: name, token: token);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<String> getToken() async {
    try {
      String token = preferences.getString(tokenKey)!;
      return token;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool> isLogin() async {
    try {
      String token = preferences.getString(tokenKey) ?? '';
      if (token.isEmpty) {
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  Future<bool> clearSession() async {
    try {
      await preferences.clear();
      return true;
    } catch (e) {
      return false;
    }
  }
}
