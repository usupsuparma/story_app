import 'package:shared_preferences/shared_preferences.dart';
import 'package:story_app/db/session_repository.dart';
import 'package:story_app/models/user.dart';
import 'package:story_app/services/api_services.dart';

class AuthRepository {
  final String stateKey = "state";
  final String userKey = "user";
  final SessionRepository sessionRepository;
  final ApiService apiService;
  AuthRepository({
    required this.sessionRepository,
    required this.apiService,
  });

  Future<bool> isLoggedIn() async {
    try {
      return sessionRepository.isLogin();
    } catch (e) {
      return false;
    }
  }

  Future<bool> login(User user) async {
    try {
      var res = await apiService.login(user);
      if (res.error) {
        return false;
      }
      await sessionRepository.createSession(res.session);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> logout() async {
    return await sessionRepository.clearSession();
  }

  Future<bool> saveUser(User user) async {
    try {
      var res = await apiService.register(user);
      if (res.error) {
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<User?> getUser() async {
    final preferences = await SharedPreferences.getInstance();
    await Future.delayed(const Duration(seconds: 2));
    final json = preferences.getString(userKey) ?? "";
    User? user;
    try {
      user = User.fromJson(json);
    } catch (e) {
      user = null;
    }
    return user;
  }
}
