import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:story_app/db/session_repository.dart';
import 'package:story_app/models/user.dart';
import 'package:story_app/responses/response_login.dart';
import 'package:story_app/responses/response_story.dart';

class ApiService {
  String baseUrl = 'https://story-api.dicoding.dev/v1';
  final http.Client client;
  final SessionRepository sessionRepository;

  ApiService({
    required this.client,
    required this.sessionRepository,
  });

  Future<ResponseResult> register(User user) async {
    try {
      String url = "$baseUrl/register";
      final headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      };
      final response = await client.post(Uri.parse(url),
          body: user.toJson(), headers: headers);
      return responseResultFromJson(response.body);
    } catch (e, stackrace) {
      print(e.toString());
      print(stackrace);
      rethrow;
    }
  }

  Future<ResponseSessionResult> login(User user) async {
    try {
      String url = "$baseUrl/login";
      final headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      };
      final response = await client.post(
        Uri.parse(url),
        body: jsonEncode({
          'email': user.email,
          'password': user.password,
        }),
        headers: headers,
      );
      return responseSessionResultFromJson(response.body);
    } catch (e, stackrace) {
      if (kDebugMode) {
        print(e.toString());
        print(stackrace);
      }
      rethrow;
    }
  }

  Future<ResponseStoriesResult> getAllStories() async {
    try {
      String url = "$baseUrl/stories?page=1&size=15";
      String token = await sessionRepository.getToken();
      final headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
      final response = await client.get(Uri.parse(url), headers: headers);
      final ResponseStoriesResult responseResult =
          ResponseStoriesResult.fromJson(
        response.body,
      );
      return responseResult;
    } catch (e, stackrace) {
      if (kDebugMode) {
        print(e.toString());
        print(stackrace);
      }
      rethrow;
    }
  }

  Future<ResponseStoryResult> getDetailStory(String id) async {
    try {
      String url = "$baseUrl/stories/$id";
      String token = await sessionRepository.getToken();
      final headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
      final response = await client.get(Uri.parse(url), headers: headers);
      var json = jsonEncode(response);
      final ResponseStoryResult responseResult = ResponseStoryResult.fromJson(
        json,
      );
      return responseResult;
    } catch (e) {
      rethrow;
    }
  }

  Future<ResponseResult> addNewStory(
    List<int> bytes,
    String fileName,
    String description,
  ) async {
    String url = "$baseUrl/stories";

    final uri = Uri.parse(url);
    var request = http.MultipartRequest('POST', uri);

    String token = await sessionRepository.getToken();
    final multiPartFile = http.MultipartFile.fromBytes(
      "photo",
      bytes,
      filename: fileName,
    );
    final Map<String, String> fields = {
      "description": description,
    };
    final Map<String, String> headers = {
      "Content-type": "multipart/form-data",
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    request.files.add(multiPartFile);
    request.fields.addAll(fields);
    request.headers.addAll(headers);

    final http.StreamedResponse streamedResponse = await request.send();
    final int statusCode = streamedResponse.statusCode;

    final Uint8List responseList = await streamedResponse.stream.toBytes();
    final String responseData = String.fromCharCodes(responseList);

    if (statusCode == 201) {
      return responseResultFromJson(responseData);
    } else {
      throw Exception("Upload file error");
    }
  }
}
