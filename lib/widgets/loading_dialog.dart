import 'package:flutter/material.dart';

class LoadingDialog {
  static showLoadingDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible:
          false, // Prevent the user from dismissing the dialog by tapping outside
      builder: (BuildContext context) {
        return const AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CircularProgressIndicator(), // Loading spinner
              SizedBox(height: 16.0), // Spacer
              Text("Loading..."), // Text to indicate loading
            ],
          ),
        );
      },
    );
  }

  static hideLoadingDialog(BuildContext context) {
    Navigator.of(context, rootNavigator: true)
        .pop(); // Close the loading dialog
  }
}
