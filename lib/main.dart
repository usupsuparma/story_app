import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:story_app/db/auth_repository.dart';
import 'package:story_app/db/session_repository.dart';
import 'package:story_app/db/story_repository.dart';
import 'package:story_app/providers/auth_provider.dart';
import 'package:story_app/providers/new_story_provider.dart';
import 'package:story_app/providers/story_provider.dart';
import 'package:story_app/routes/route_information_parser.dart';
import 'package:story_app/routes/router_delegate.dart';
import 'package:story_app/services/api_services.dart';
import 'package:http/http.dart' as http;

void main() async {
  runApp(const StoryApp());
}

class StoryApp extends StatefulWidget {
  const StoryApp({super.key});

  @override
  State<StoryApp> createState() => _StoryAppState();
}

class _StoryAppState extends State<StoryApp> {
  late MyRouterDelegate myRouterDelegate;
  late MyRouteInformationParser myRouteInformationParser;
  late AuthProvider authProvider;
  late NewStoryProvider newStoryProvider;
  late SessionRepository sessionRepository;
  late SessionRepositoryImpl sessionRepositoryImpl;
  late SharedPreferences preferences;
  late ApiService apiService;
  late StoryProvider storyProvider;
  late StoryRepository storyRepository;
  late StoryRepositoryImpl storyRepositoryImpl;
  @override
  void initState() {
    super.initState();
    initializeLibrary();
  }

  Future<void> initializeLibrary() async {
    final preferences = await SharedPreferences.getInstance();
    sessionRepositoryImpl = SessionRepositoryImpl(preferences: preferences);
    sessionRepository = sessionRepositoryImpl;

    apiService =
        ApiService(client: http.Client(), sessionRepository: sessionRepository);

    final authRepository = AuthRepository(
        sessionRepository: sessionRepository, apiService: apiService);
    authProvider = AuthProvider(authRepository);

    storyRepositoryImpl = StoryRepositoryImpl(apiService: apiService);
    storyRepository = storyRepositoryImpl;

    newStoryProvider = NewStoryProvider(storyRepository: storyRepository);
    storyProvider = StoryProvider(storyRepository: storyRepository);

    myRouterDelegate = MyRouterDelegate(authRepository);
    myRouteInformationParser = MyRouteInformationParser();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: initializeLibrary(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (context) => authProvider,
              ),
              ChangeNotifierProvider(
                create: (context) => newStoryProvider,
              ),
              ChangeNotifierProvider(
                create: (context) => storyProvider,
              ),
            ],
            child: MaterialApp.router(
              title: 'Story App',
              routerDelegate: myRouterDelegate,
              routeInformationParser: myRouteInformationParser,
              backButtonDispatcher: RootBackButtonDispatcher(),
            ),
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}
