import 'dart:convert';
import 'package:story_app/models/story.dart';
import 'package:story_app/responses/response_base.dart';

class ResponseStoriesResult extends ResponseBase {
  final List<Story> listStory;

  ResponseStoriesResult({
    required bool error,
    required String message,
    required this.listStory,
  }) : super(error: error, message: message);

  factory ResponseStoriesResult.fromJson(String source) {
    final Map<String, dynamic> jsonMap = json.decode(source);
    return ResponseStoriesResult(
      error: jsonMap['error'] ?? false,
      message: jsonMap['message'] ?? '',
      listStory: List<Story>.from(
        jsonMap['listStory'].map((x) => Story.fromJson(x)),
      ),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "error": error,
      "message": message,
      "listStory": List<dynamic>.from(listStory.map((x) => x.toJson())),
    };
    return data;
  }
}

class ResponseStoryResult extends ResponseBase {
  final Story story;

  ResponseStoryResult({
    required bool error,
    required String message,
    required this.story,
  }) : super(error: error, message: message);

  factory ResponseStoryResult.fromJson(String source) {
    final Map<String, dynamic> jsonMap = json.decode(source);
    return ResponseStoryResult(
      error: jsonMap['error'] ?? false,
      message: jsonMap['message'] ?? '',
      story: Story.fromJson(jsonMap["story"]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "error": error,
      "message": message,
      "story": story.toJson(),
    };
    return data;
  }
}

ResponseResult responseResultFromJson(String str) =>
    ResponseResult.fromJson(json.decode(str));

class ResponseResult extends ResponseBase {
  ResponseResult({
    required bool error,
    required String message,
  }) : super(error: error, message: message);

  factory ResponseResult.fromJson(Map<String, dynamic> json) => ResponseResult(
        error: json["error"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
      };
}
