abstract class ResponseBase {
  final bool error;
  final String message;

  ResponseBase({
    required this.error,
    required this.message,
  });
}
