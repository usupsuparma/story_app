import 'dart:convert';

import 'package:story_app/models/session.dart';
import 'package:story_app/responses/response_base.dart';

ResponseSessionResult responseSessionResultFromJson(String str) =>
    ResponseSessionResult.fromJson(json.decode(str));

class ResponseSessionResult extends ResponseBase {
  final Session session;

  ResponseSessionResult({
    required bool error,
    required String message,
    required this.session,
  }) : super(error: error, message: message);

  factory ResponseSessionResult.fromJson(Map<String, dynamic> json) =>
      ResponseSessionResult(
        error: json["error"],
        message: json["message"],
        session: Session.fromJson(json["loginResult"]),
      );
}
