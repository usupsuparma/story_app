import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:story_app/db/story_repository.dart';
import 'package:story_app/responses/response_story.dart';

class NewStoryProvider extends ChangeNotifier {
  StoryRepository storyRepository;
  NewStoryProvider({required this.storyRepository});
  String? imagePath;

  /// todo-gallery-04: create a image variable so save image information
  XFile? imageFile;

  void setImagePath(String? value) {
    imagePath = value;
    notifyListeners();
  }

  /// todo-gallery-04-02: create a function to save a value
  void setImageFile(XFile? value) {
    imageFile = value;
    notifyListeners();
  }

  bool isUploading = false;
  String message = "";
  ResponseResult? uploadResponse;

  Future<void> upload(
    List<int> bytes,
    String fileName,
    String description,
  ) async {
    try {
      message = "";
      uploadResponse = null;
      isUploading = true;
      notifyListeners();
      uploadResponse =
          await storyRepository.addNewStory(bytes, fileName, description);
      message = uploadResponse?.message ?? 'success';
      isUploading = false;
      notifyListeners();
    } catch (e, stackrace) {
      print(stackrace);
      isUploading = false;
      message = e.toString();
      notifyListeners();
    }
  }

  Future<List<int>> compressImage(Uint8List bytes) async {
    int imageLength = bytes.length;
    if (imageLength < 1000000) return bytes;
    final img.Image image = img.decodeImage(bytes)!;
    int compressQuality = 100;
    int length = imageLength;
    List<int> newByte = [];
    do {
      ///
      compressQuality -= 10;
      newByte = img.encodeJpg(
        image,
        quality: compressQuality,
      );
      length = newByte.length;
    } while (length > 1000000);
    return newByte;
  }
}
