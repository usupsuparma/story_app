import 'package:flutter/material.dart';
import 'package:story_app/db/story_repository.dart';
import 'package:story_app/models/story.dart';

class StoryProvider extends ChangeNotifier {
  StoryRepository storyRepository;
  StoryProvider({required this.storyRepository});

  bool _isLoadingStories = false;
  bool get isLoadingStories => _isLoadingStories;
  List<Story> _stories = [];
  List<Story> get stories => _stories;

  Future<void> fetchStories() async {
    try {
      _isLoadingStories = true;
      notifyListeners();
      _stories = await storyRepository.getStories();
      _isLoadingStories = false;
      notifyListeners();
    } catch (e) {
      _isLoadingStories = false;
      notifyListeners();
    }
  }
}
