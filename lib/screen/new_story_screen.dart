import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:story_app/providers/new_story_provider.dart';
import 'package:story_app/widgets/loading_dialog.dart';

class NewStorScreen extends StatefulWidget {
  final Function() onBack;
  const NewStorScreen({super.key, required this.onBack});

  @override
  State<NewStorScreen> createState() => _NewStorScreenState();
}

class _NewStorScreenState extends State<NewStorScreen> {
  _onUpload() async {
    try {
      final newStoryProvider =
          Provider.of<NewStoryProvider>(context, listen: false);
      final imagePath = newStoryProvider.imagePath;
      final imageFile = newStoryProvider.imageFile;
      if (imagePath == null || imageFile == null) return;
      final fileName = imageFile.name;
      final bytes = await imageFile.readAsBytes();
      final newBytes = await newStoryProvider.compressImage(bytes);
      // ignore: use_build_context_synchronously
      LoadingDialog.showLoadingDialog(context);
      await newStoryProvider.upload(
        newBytes,
        fileName,
        _descriptionController.text,
      );
      // ignore: use_build_context_synchronously
      LoadingDialog.hideLoadingDialog(context);
      final ScaffoldMessengerState scaffoldMessengerState =
          // ignore: use_build_context_synchronously
          ScaffoldMessenger.of(context);
      if (newStoryProvider.uploadResponse != null) {
        newStoryProvider.setImageFile(null);
        newStoryProvider.setImagePath(null);
        scaffoldMessengerState.showSnackBar(
          SnackBar(content: Text(newStoryProvider.message)),
        );
        widget.onBack();
      }
    } catch (e, stackrace) {
      if (kDebugMode) {
        print(e.toString());
        print(stackrace);
      }
    }
  }

  _onGalleryView() async {
    final isMacOS = defaultTargetPlatform == TargetPlatform.macOS;
    final isLinux = defaultTargetPlatform == TargetPlatform.linux;
    if (isMacOS || isLinux) return;
    final ImagePicker picker = ImagePicker();
    final provider = context.read<NewStoryProvider>();

    final XFile? pickerFile =
        await picker.pickImage(source: ImageSource.gallery);

    if (pickerFile != null) {
      provider.setImageFile(pickerFile);
      provider.setImagePath(pickerFile.path);
    }
  }

  _onCameraView() async {
    final provider = context.read<NewStoryProvider>();
    final isAndroid = defaultTargetPlatform == TargetPlatform.android;
    final isiOS = defaultTargetPlatform == TargetPlatform.iOS;
    final isNotMobile = !(isAndroid || isiOS);
    if (isNotMobile) return;
    final ImagePicker picker = ImagePicker();
    final XFile? pickerFile =
        await picker.pickImage(source: ImageSource.camera);
    if (pickerFile != null) {
      provider.setImageFile(pickerFile);
      provider.setImagePath(pickerFile.path);
    }
  }

  Widget _showImage() {
    final imagePath = context.read<NewStoryProvider>().imagePath;
    return kIsWeb
        ? Image.network(
            imagePath.toString(),
            fit: BoxFit.contain,
          )
        : Image.file(
            File(imagePath.toString()),
            fit: BoxFit.contain,
          );
  }

  final TextEditingController _descriptionController = TextEditingController();

  @override
  void dispose() {
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("New Story"),
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 3,
              child: context.watch<NewStoryProvider>().imagePath == null
                  ? Align(
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.image,
                        size: MediaQuery.of(context).size.width / 2,
                      ),
                    )
                  : _showImage(),
            ),
            const SizedBox(
              height: 16,
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () => _onGalleryView(),
                      child: const Text('Gallery')),
                  ElevatedButton(
                      onPressed: () => _onCameraView(),
                      child: const Text('Camera')),
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              controller:
                  _descriptionController, // Menggunakan controller yang telah dibuat
              decoration: const InputDecoration(
                fillColor: Colors.white,
                labelText: 'Masukkan teks di sini',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16))),
              ),
              maxLines: 5,
            ),
            const SizedBox(
              height: 16,
            ),
            ElevatedButton(
              onPressed: _onUpload,
              child: const Text('Upload'),
            ),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      )),
    );
  }
}
