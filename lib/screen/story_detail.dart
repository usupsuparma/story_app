import 'package:flutter/material.dart';

class StoryDetailScreen extends StatefulWidget {
  final String storyId;
  const StoryDetailScreen({
    super.key,
    required this.storyId,
  });

  @override
  State<StoryDetailScreen> createState() => _StoryDetailScreenState();
}

class _StoryDetailScreenState extends State<StoryDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
