import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:story_app/models/story.dart';
import 'package:story_app/providers/auth_provider.dart';
import 'package:story_app/providers/story_provider.dart';

class StoryListScreen extends StatefulWidget {
  final Function(String) onTapped;
  final Function() onLogout;
  final Function() onAdd;

  const StoryListScreen({
    Key? key,
    required this.onTapped,
    required this.onLogout,
    required this.onAdd,
  }) : super(key: key);

  @override
  State<StoryListScreen> createState() => _StoryListScreenState();
}

class _StoryListScreenState extends State<StoryListScreen> {
  late List<Story> stories;

  @override
  void initState() {
    super.initState();
    stories = [];
    Future.microtask(() =>
        Provider.of<StoryProvider>(context, listen: false).fetchStories());
  }

  @override
  Widget build(BuildContext context) {
    final authWatch = context.watch<AuthProvider>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Storys App"),
        actions: [
          authWatch.isLoadingLogout
              ? const CircularProgressIndicator(
                  color: Colors.white,
                )
              : IconButton(
                  onPressed: () async {
                    final authRead = context.read<AuthProvider>();
                    final result = await authRead.logout();
                    if (result) widget.onLogout();
                  },
                  icon: const Icon(Icons.logout),
                ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 16,
              ),
              Consumer<StoryProvider>(
                builder: (context, value, child) {
                  if (value.isLoadingStories) {
                    return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: const Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }

                  List<Story> stories = value.stories;
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: stories.map((Story story) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.network(
                              story.photoUrl,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.width / 2,
                              fit: BoxFit.cover,
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Text(story.name),
                            const SizedBox(
                              height: 8,
                            ),
                            Text(story.description),
                            const SizedBox(
                              height: 16,
                            ),
                          ],
                        );
                      }).toList(),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => widget.onAdd(),
        tooltip: "Add",
        child: const Icon(Icons.add),
      ),
    );
  }
}
